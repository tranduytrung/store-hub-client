/// <reference path="../index.ts" />

module StoreHub {
    export module Services {
        import ng = angular;
        var db = localforage.createInstance({ name: "auth" });
        var TOKEN_KEY = "auth_token";

        export class AuthService {
            tokenExipiry: Date

            constructor(
                private $state: ng.ui.IStateService,
                private $window: ng.IWindowService,
                private $http: ng.IHttpService,
                private $log: ng.ILogService) {
            }

            async getToken(login: boolean = true) {
                var authToken = URI(this.$window.location.href).query(true)["token"];
                if (authToken) {
                    await db.setItem(TOKEN_KEY, authToken);
                } else {
                    authToken = await db.getItem<string>(TOKEN_KEY);
                }

                if (authToken && !this.tokenExipiry) {
                    try {
                        let result = await this.$http.get<string>(StoreHub.host + "/auth/status", { params: { token: authToken } });
                        this.tokenExipiry = new Date(Number(result.data));
                    }
                    catch (e) {
                        authToken = null;
                    }
                }

                if (!authToken) {
                    if (StoreHub.isTest) {
                        authToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1Nzg4ZjMwOWY3OTQwOGRlMDZmY2RiM2EiLCJlbWFpbCI6InRyYW5kdXl0cnVuZ0BvdXRsb29rLmNvbSIsImZhY2Vib29rSWQiOiIxMjc1NTk1OTA1Nzg1MjE3IiwiZmlyc3ROYW1lIjoiVHLhuqduIiwibGFzdE5hbWUiOiJUcnVuZyIsImlhdCI6MTQ3Mzg1NzE2NSwiZXhwIjoxNDc5MDQxMTY1fQ.b8isZQpau1a6A_J5OHqdzvPrBFZA63GIFRLuyyKdK4A";
                    } else if (login) {
                        authToken = await this.facebook();
                        await db.setItem(TOKEN_KEY, authToken);
                    }
                }

                return authToken;
            }

            async clearCache() {
                await db.removeItem(TOKEN_KEY);
            }

            facebook(): Promise<string> {
                var url = StoreHub.host + "/auth/facebook";
                if (!window.cordova) {
                    let redirect = this.$state.href(this.$state.current, this.$state.params, { absolute: true });
                    this.$window.location.href = url + "?redirect=" + encodeURIComponent(redirect);
                    return Promise.resolve(null);
                }

                return new Promise<string>((resolve, reject) => {
                    let redirect = "http://localhost/callback";
                    let InAppBrowser: InAppBrowser = window.cordova["InAppBrowser"];
                    let browser = InAppBrowser.open(url + "?redirect=" + encodeURIComponent(redirect), '_blank', 'location=no,clearsessioncache=yes,clearcache=yes');

                    var onExit = () => {
                        reject(new Error("logging in facebook is terminated"));
                    }

                    var onLoadStart = (event: InAppBrowserEvent) => {
                        if (event.url.indexOf(redirect) !== 0) return;
                        browser.removeEventListener("exit", onExit);
                        browser.close();

                        var query = URI(event.url).query(true);
                        if (query["token"]) {
                            resolve(query["token"]);
                        } else {
                            reject(new Error(query["error"] || "unexpected error"));
                        }
                    }

                    browser.addEventListener("exit", onExit);
                    browser.addEventListener("loadstart", onLoadStart);
                });
            }

        }

        storehub.service("AuthService", [
            "$state", "$window", "$http", "$log", AuthService
        ]);
    }
}