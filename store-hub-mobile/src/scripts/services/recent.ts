// Recent access service

module StoreHub {
    export module Services {
        import ng = angular;
        var db = localforage.createInstance({name: "recent", storeName: "recent"});
        var indexDate = localforage.createInstance({name: "recent", storeName: "index_date"});

        export class RecentService {
            constructor(
                private $log: ng.ILogService) {
            }

            async set(id : string) {
                var oldItem = await db.getItem<IRecentItem>(id);
                if (oldItem) {
                    await indexDate.removeItem(oldItem.date.getTime().toString());
                }

                var date = new Date();
                await db.setItem(id, {
                    id: id,
                    date: date
                });

                await indexDate.setItem(date.getTime().toString(), id);
            }

            async remove(id : string) {
                var obj = await db.getItem<IRecentItem>(id);
                await db.removeItem(id);
                await indexDate.removeItem(obj.date.getTime().toString());
            }

            async clear() {
                await db.clear();
                await indexDate.clear();
            }

            async getRecentItems(skip : number = 0, limit : number = 20) {
                var result : IRecentItem[] = [];
                var length = await db.length();
                var start = Util.clamp(length - 1 - skip, -1, length - 1);
                var end = Util.clamp(start - limit + 1, 0, length);

                for (var index = start; index >= end; index--) {
                    var time = await indexDate.key(index);
                    var id = await indexDate.getItem<string>(time);
                    var date = new Date(Number(time));

                    result.push({
                        date,
                        id
                    });
                }

                return result;
            }

            count() {
                return db.length();
            }
        }

        storehub.service("RecentService", [
            "$log", RecentService
        ]);

        interface IRecentItem {
            id : string,
            date : Date
        }
    }
}