module StoreHub {
    export module Services {
        import md = angular.material;
        import ng = angular;

        export class ProductService {
            settings: IProductServiceSettings = {
                search: {
                    sources: null,
                    sort: Models.ProductSortType.relevance
                }
            }

            constructor(private $http: ng.IHttpService) {

            }

            async getNewArrivals(lastId?: string) {
                var result = await this.$http.get<Models.IProduct[]>(StoreHub.host + "/product/getNewArrivals", {
                    params: {
                        lastId: lastId,
                        pageSize: 25
                    }
                });
                return result.data;
            }

            async getProduct(id: string) {
                var result = await this.$http.get<Models.IProduct>(StoreHub.host + "/product", {
                    params: {
                        id: id
                    }
                });

                var product = result.data;
                product.prices = this.sortPricePoints(product.prices);
                return this.formatDate<Models.IProduct>(product);
            }

            async getProductsByGroupId(id: string, pageIndex: number = 0, pageSize: number = 6) {
                var result = await this.$http.get<Models.IProduct[]>(StoreHub.host + "/product/byGroupId", {
                    params: {
                        id: id,
                        pageIndex: pageIndex,
                        pageSize: pageSize
                    }
                });

                var products = result.data;
                _.forEach(products, (product) => {
                    this.formatDate<Models.IProduct>(product);
                    product.prices = this.sortPricePoints(product.prices);
                });

                return products;
            }

            async getProductByUrl(url: string) {
                var result = await this.$http.get<Models.IProductBrief>(StoreHub.host + "/product/byUrl", {
                    params: {
                        url
                    }
                });

                return this.formatDate<Models.IProductBrief>(result.data);
            }

            async getProducts(ids: string[]): Promise<Models.IProductBrief[]> {
                if (_.isEmpty(ids)) {
                    return [];
                }

                var result = await this.$http.get<Models.IProductBrief[]>(StoreHub.host + "/product", {
                    params: {
                        ids: ids
                    }
                });

                return result.data;
            }

            async search(options: ISearchOptions) {
                options = _.defaults<ISearchOptions, ISearchOptions>(options, {
                    q: null,
                    categories: null,
                    sources: this.settings.search.sources,
                    sort: this.settings.search.sort,
                    skip: null,
                    limit: null
                });
                var result = await this.$http.get<Models.IProductGroup[]>(StoreHub.host + "/product/search", {
                    params: options
                });

                var groups = result.data;
                this.sortByPrice(groups);

                return groups;
            }

            private sortByPrice(groups: Models.IProductGroup[]) {
                groups.forEach(function (group, index) {
                    groups[index].products = _.orderBy(group.products, "price");
                });
            }

            private formatDate<T>(product: any): T {
                if (product.createdDate) product.createdDate = new Date(product.createdDate);
                if (product.updatedDate) product.updatedDate = new Date(product.updatedDate);
                if (product.prices) {
                    _.forEach(product.prices, function (price) {
                        price.date = new Date(price.date);
                    });
                }

                return product;
            }

            private sortPricePoints(prices : Models.IPricePoint[]) {
                return _.orderBy(prices, ["date"], ["desc"]);
            }
        }

        storehub.service("ProductService", [
            "$http", ProductService
        ]);

        interface IProductServiceSettings {
            search: ISearchSettings
        }

        interface ISearchSettings {
            sources: string[]
            sort: Models.ProductSortType
        }

        interface ISearchOptions {
            q?: string
            categories?: Models.ProductCategory[]
            sources?: string[]
            sort?: Models.ProductSortType
            skip?: number
            limit?: number
        }
    }
}