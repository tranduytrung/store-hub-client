/// <reference path="../index.ts" />

module StoreHub {
    export module Services {
        import md = angular.material;
        import ng = angular;

        export class UserService {
            user: Models.IUser
            constructor(
                private $http: ng.IHttpService,
                private $log: ng.ILogService,
                private AuthService: Services.AuthService) {

            }

            async getUser(login : boolean = true): Promise<Models.IUser> {
                if (!_.isEmpty(this.user)) {
                    return this.user;
                }

                var token = await this.AuthService.getToken(login);
                if (_.isEmpty(token)) return null; 
                let result = await this.$http.get<Models.IUser>(StoreHub.host + "/user", {
                    params: {
                        token
                    }
                });

                this.user = result.data;
                let onesignalId = await OneSignal.getOnesignalId().catch(_.noop);
                // unable to get onesignalId
                // may be internet connection issue or user refuse to be notified
                if (_.isEmpty(onesignalId)) {
                    return this.user;
                }

                if (!_.includes(this.user.onesignalIds, onesignalId)) {
                    await this.addOnesignal(onesignalId);
                }

                return this.getUser();
            }

            async logout() {
                await this.AuthService.clearCache();
                this.user = null;
                this.$log.info("user logged out");
            }

            async track(item: Models.IWishItem) {
                var user = await this.getUser();
                var isExisted = _.findIndex(user.wishlist, ["productId", item.productId]) >= 0;

                var path = isExisted ? "/user/updateWishItem" : "/user/addWishItem";
                await this.$http.post<Models.IUser>(StoreHub.host + path,
                    {
                        wishItem: item
                    },
                    {
                        headers: {
                            Authorization: await this.AuthService.getToken()
                        }
                    }
                );

                // force to reset user
                this.user = null;
            }
            
            async untrack(productId: string) {
                var user = await this.getUser();
                var wishItem = _.find(user.wishlist, ["productId", productId]);

                if (!(wishItem && wishItem.track)) {
                    return;
                }

                await this.$http.post<Models.IUser>(StoreHub.host + "/user/updateWishItem",
                    {
                        wishItem: { productId: productId, track: null }
                    },
                    {
                        headers: {
                            Authorization: await this.AuthService.getToken()
                        }
                    }
                );

                // force to reset user
                this.user = null;
            }

            async wish(productId: string) {
                var user = await this.getUser();
                var isExisted = _.findIndex(user.wishlist, ["productId", productId]) >= 0;

                if (isExisted) {
                    return;
                }

                await this.$http.post<Models.IUser>(StoreHub.host + "/user/addWishItem",
                    {
                        wishItem: { productId: productId, track: null }
                    },
                    {
                        headers: {
                            Authorization: await this.AuthService.getToken()
                        }
                    }
                );

                // force to reset user
                this.user = null;
            }

            async unwish(productId: string) {
                var user = await this.getUser();
                var isExisted = _.findIndex(user.wishlist, ["productId", productId]) >= 0;

                if (!isExisted) {
                    return;
                }

                await this.$http.post<Models.IUser>(StoreHub.host + "/user/removeWishItem",
                    {
                        productId: productId
                    },
                    {
                        headers: {
                            Authorization: await this.AuthService.getToken()
                        }
                    }
                );

                // force to reset user
                this.user = null;
            }

            async addOnesignal(onesignalId: string) {
                await this.$http.post<Models.IUser>(StoreHub.host + "/user/addOnesignal",
                    {
                        onesignalId: onesignalId
                    },
                    {
                        headers: {
                            Authorization: await this.AuthService.getToken()
                        }
                    }
                );

                // force to reset user
                this.user = null;
            }
        }

        storehub.service("UserService", [
            "$http", "$log", "AuthService", UserService
        ]);
    }
}