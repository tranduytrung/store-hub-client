module StoreHub {
    export module Services {
        import ng = angular;
        import md = angular.material;
        export class AsyncService {
            private semaphore = new Util.Semaphore(1);
            private defaultToastPreset: md.ISimpleToastPreset
            private fatalDialogPreset: md.IConfirmDialog

            constructor(
                private $log: ng.ILogService,
                private $window: ng.IWindowService,
                private $mdToast: md.IToastService,
                private $mdDialog: md.IDialogService) {

                this.defaultToastPreset = $mdToast.simple()
                    .position("bottom right");

                this.fatalDialogPreset = $mdDialog.confirm()
                    .title("Would you like to refresh this page?")
                    .textContent("A fatal error has occurred and likely cannot be recovered.")
                    .ok("Please do it!")
                    .cancel("No, it still seem good");
            }

            run<R>(action: IAsyncAction<R>, options?: IAsyncOptions): Promise<R> {
                options = _.defaults<IAsyncOptions, IAsyncOptions>(options, {
                    runningText: "loading...",
                    errorText: "unexpected error occurred"
                });

                return this.semaphore.lock<R>(async () => {
                    this.$mdToast.show(this.defaultToastPreset.textContent(options.runningText).hideDelay(0));
                    var result: R;
                    try {
                        result = await action();
                        this.$mdToast.hide();
                        if (options.successText) {
                            this.$mdToast.show(this.defaultToastPreset.textContent(options.successText).hideDelay(3000));
                        }
                    } catch (error) {
                        if (options.errorText) {
                            this.$mdToast.show(this.defaultToastPreset.textContent(options.errorText).hideDelay(3000));
                        }

                        if (error instanceof FatalError) {
                            this.$mdDialog.show(this.fatalDialogPreset)
                                .then(() => {
                                    this.$window.location.reload();
                                });
                        }

                        throw error;
                    }

                    return result;
                });
            }
        }

        export class FatalError extends Error {
        }

        interface IAsyncAction<R> {
            (): Promise<R>
        }

        interface IAsyncOptions {
            runningText?: string
            successText?: string
            errorText?: string
        }

        storehub.service("AsyncService", [
            "$log", "$window", "$mdToast", "$mdDialog", AsyncService
        ]);
    }
}