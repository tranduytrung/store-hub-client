module StoreHub {
    export module Services {
        import ng = angular;
        export class AdService {
            constructor(
                private $log: ng.ILogService,
                private $http: ng.IHttpService) {

            }

            async getSourceEnumeration() {
                var result = await this.$http.get<string[]>(StoreHub.host + "/ad/getSourceEnumeration");
                this.$log.info("getSourceEnumeration", result.data);
                return result.data;
            }

            async getAds(source: string) {
                var result = await this.$http.get<Models.IAd[]>(StoreHub.host + "/ad/bySource", {
                    params: { source: source }
                });
                this.$log.info("getAds", result.data);
                return result.data;
            }
        }

        storehub.service("AdService", [
            "$log", "$http", AdService
        ]);
    }
}