// Recent access service

module StoreHub {
    export module Services {
        import ng = angular;
        import md = angular.material;

        var db = localforage.createInstance({ name: "setting" });
        const THEME_KEY = "theme";
        var currentThemeId: string;
        var themeSet: IThemeObject[] = [];

        export class ThemeService {
            current: IThemeObject

            constructor(
                private $log: ng.ILogService,
                private $window: ng.IWindowService) {
                this.current = _.find(themeSet, ["id", currentThemeId]);
            }

            getThemes() {
                return themeSet;
            }

            async setTheme(id: string) {
                await db.setItem(THEME_KEY, id);
                this.$window.location.reload();
            }
        }

        storehub.service("ThemeService", [
            "$log", "$window", ThemeService
        ]);

        Application.init(async function () {
            currentThemeId = (await db.getItem<string>(THEME_KEY)) || "white";
            storehub.config(['$mdThemingProvider',
                function ($mdThemingProvider: md.IThemingProvider) {
                    var darkGreyPalette = $mdThemingProvider.extendPalette("grey", {
                        "900": "181818",
                        "800": "222222",
                        "700": "363636",
                        "600": "424242"
                    });

                    $mdThemingProvider.definePalette("dark-grey", darkGreyPalette);

                    themeSet.push({
                        id: "night",
                        name: "Night",
                        theme: $mdThemingProvider.theme("night")
                            .backgroundPalette("dark-grey", {
                                "default": "900",
                                "hue-1": "800",
                                "hue-2": "700"
                            })
                            .primaryPalette("dark-grey", {
                                "default": "800",
                                "hue-1": "700",
                                "hue-2": "600"
                            })
                            .warnPalette("red")
                            .accentPalette("deep-orange", {
                                "hue-1": "400"
                            })
                            .dark()
                    });

                    themeSet.push({
                        id: "grey",
                        name: "Grey",
                        theme: $mdThemingProvider.theme("grey")
                            .backgroundPalette("grey", {
                                "default": "100"
                            })
                            .primaryPalette("grey", {
                                "default": "400",
                                "hue-1": "300"
                            })
                            .warnPalette("red")
                            .accentPalette("blue-grey", {
                                "default": "500",
                                "hue-1": "300"
                            })
                    });

                    themeSet.push({
                        id: "white",
                        name: "White",
                        theme: $mdThemingProvider.theme("white")
                            .backgroundPalette("grey", {
                                "default": "50"
                            })
                            .primaryPalette("grey", {
                                "default": "200",
                                "hue-1": "100"
                            })
                            .warnPalette("red")
                            .accentPalette("red", {
                                "default": "500",
                                "hue-1": "300"
                            })
                    });

                    themeSet.push({
                        id: "red",
                        name: "Red",
                        theme: $mdThemingProvider.theme("red")
                            .backgroundPalette("red", {
                                "default": "50"
                            })
                            .primaryPalette("red", {
                                "default": "700",
                                "hue-1": "200"
                            })
                            .warnPalette("grey")
                            .accentPalette("lime", {
                                "default": "500",
                                "hue-1": "300"
                            })
                    });

                    themeSet.push({
                        id: "green",
                        name: "Green",
                        theme: $mdThemingProvider.theme("green")
                            .backgroundPalette("green", {
                                "default": "50"
                            })
                            .primaryPalette("green", {
                                "default": "700",
                                "hue-1": "200"
                            })
                            .warnPalette("red")
                            .accentPalette("deep-orange", {
                                "default": "500",
                                "hue-1": "300"
                            })
                    });

                    themeSet.push({
                        id: "brown",
                        name: "Brown",
                        theme: $mdThemingProvider.theme("brown")
                            .backgroundPalette("brown")
                            .primaryPalette("brown", {
                                "hue-1": "400"
                            })
                            .warnPalette("red")
                            .accentPalette("orange")
                    });

                    $mdThemingProvider.setDefaultTheme(currentThemeId);
                }]
            );
        });

        export interface IThemeObject {
            id: string
            theme: md.ITheme
            name: string
        }
    }
}