/// <reference path="../index.ts" />

module StoreHub {
    module ScrollBottom {
        import md = angular.material;
        import ng = angular;

        storehub.directive("scrollBottom", ["$parse",
            function($parse: ng.IParseService) {
                return {
                    link(scope : ng.IScope, element : ng.IAugmentedJQuery, attrs : ng.IAttributes) {
                        var func = $parse(attrs["scrollBottom"]);
                        element.on("scroll", _.throttle(function() {
                            var scrollHeight = $(element)[0].scrollHeight;
                            var offset = Util.clamp(scrollHeight*0.8, 0, 400);
                            if($(element).scrollTop() + $(element).innerHeight() >= scrollHeight - offset) {
                                func(scope);
                            }
                        }, 200));
                    }
                };
            }
        ]);
    }
}