module tranduytrung.directives {
    module mdThemed {
        import ng = angular;
        import md = angular.material;

        var module = ng.module("mdThemed", ["ngMaterial"]);
        module.directive("mdThemed", ["$mdTheming", function($mdTheming) {
            return {
                restrict: 'A',
                link: postLink
            };
            
            function postLink(scope, element, attr) {
                // instruct to apply theme on the element
                $mdTheming(element);
            };
        }]);

        module.config(["$MD_THEME_CSS", "$provide",
            function($mdThemeCss, $provide: ng.auto.IProvideService) {
                $provide.constant("$MD_THEME_CSS", $mdThemeCss + css);
            }
        ]);
        
                
        /* Some Example Valid Theming Expressions
        * =======================================
        *
        * Intention group expansion: (valid for primary, accent, warn, background)
        *
        * {{primary-100}} - grab shade 100 from the primary palette
        * {{primary-100-0.7}} - grab shade 100, apply opacity of 0.7
        * {{primary-100-contrast}} - grab shade 100's contrast color
        * {{primary-hue-1}} - grab the shade assigned to hue-1 from the primary palette
        * {{primary-hue-1-0.7}} - apply 0.7 opacity to primary-hue-1
        * {{primary-color}} - Generates .md-hue-1, .md-hue-2, .md-hue-3 with configured shades set for each hue
        * {{primary-color-0.7}} - Apply 0.7 opacity to each of the above rules
        * {{primary-contrast}} - Generates .md-hue-1, .md-hue-2, .md-hue-3 with configured contrast (ie. text) color shades set for each hue
        * {{primary-contrast-0.7}} - Apply 0.7 opacity to each of the above rules
        *
        * Foreground expansion: Applies rgba to black/white foreground text
        *
        * {{foreground-1}} - used for primary text
        * {{foreground-2}} - used for secondary text/divider
        * {{foreground-3}} - used for disabled text
        * {{foreground-4}} - used for dividers
        *
        */

        var css = `            
            .md-foreground.md-THEME_NAME-theme {
                color: '{{background-color}}' !important
            }
            .md-foreground.md-THEME_NAME-theme.md-primary {
                color: '{{primary-color}}' !important
            }
            .md-foreground.md-THEME_NAME-theme.md-accent {
                color: '{{accent-color}}' !important
            }
            .md-foreground.md-THEME_NAME-theme.md-warn {
                color: '{{warn-color}}' !important
            }
            
            .md-foreground-contrast.md-THEME_NAME-theme {
                color: '{{background-contrast}}' !important
            }
            .md-foreground-contrast.md-THEME_NAME-theme.md-primary {
                color: '{{primary-contrast}}' !important
            }
            .md-foreground-contrast.md-THEME_NAME-theme.md-accent {
                color: '{{accent-contrast}}' !important
            }
            .md-foreground-contrast.md-THEME_NAME-theme.md-warn {
                color: '{{warn-contrast}}' !important
            }
            
            .md-background.md-THEME_NAME-theme {
                background-color: '{{background-color}}' !important
            }            
            .md-background.md-THEME_NAME-theme.md-primary {
                background-color: '{{primary-color}}' !important
            }
            .md-background.md-THEME_NAME-theme.md-accent {
                background-color: '{{accent-color}}' !important
            }
            .md-background.md-THEME_NAME-theme.md-warn {
                background-color: '{{warn-color}}' !important
            }
            
            .md-border.md-THEME_NAME-theme {
                border-color: '{{background-color}}' !important
            }            
            .md-border.md-THEME_NAME-theme.md-primary {
                border-color: '{{primary-color}}' !important
            }
            .md-border.md-THEME_NAME-theme.md-accent {
                border-color: '{{accent-color}}' !important
            }
            .md-border.md-THEME_NAME-theme.md-warn {
                border-color: '{{warn-color}}' !important
            }
            
            .md-foreground-primary.md-THEME_NAME-theme {
                color: '{{foreground-1}}' !important
            }
            .md-foreground-secondary.md-THEME_NAME-theme {
                color: '{{foreground-2}}' !important
            }
            .md-foreground-disable.md-THEME_NAME-theme {
                color: '{{foreground-3}}' !important
            }
            .md-foreground-divider.md-THEME_NAME-theme {
                color: '{{foreground-4}}' !important
            }
            
            nvd3.md-THEME_NAME-theme .nv-currentValue {
                fill: '{{accent-color}}' !important
            }
            nvd3.md-THEME_NAME-theme {
                fill: '{{accent-hue-1}}' !important
            }

            ::-webkit-input-placeholder,::-moz-placeholder,:-ms-input-placeholder {
                color: '{{primary-contrast}}' !important
            }

            md-select-menu.md-THEME_NAME-theme md-option:focus:not([disabled]):not([selected]) {
                background-color: '{{primary-color}}' !important
            }

            md-tabs.md-THEME_NAME-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active {
                color: '{{accent-color}}'
            }

            .md-button.md-THEME_NAME-theme[disabled] md-icon {
                color: '{{primary-contrast-0.3}}' !important
            }
            `;
    }
}