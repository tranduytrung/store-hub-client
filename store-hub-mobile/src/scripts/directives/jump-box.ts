/// <reference path="../index.ts" />

module StoreHub {
    module JumpBox {
        import md = angular.material;
        import ng = angular;

        storehub.directive("mdJumpBox", 
        [function() {
                var di : ng.IDirective = {
                    template: templates["jump-box"](),
                    transclude: true,
                    scope: {},
                    bindToController: {
                        selectedIndex: "=?mdCurrentIndex"
                    },
                    controller: "JumpBoxController",
                    controllerAs: "jb"
                };
                
                return di;
            }
        ]);
    }
}