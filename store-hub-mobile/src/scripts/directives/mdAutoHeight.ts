module tranduytrung.directives {
    module mdAutoHeight {
        import md = angular.material;
        import ng = angular;

        var module = ng.module("mdAutoHeight", []);
        module.directive("mdAutoHeight",
            ["$timeout", "$window",
                function($timeout: ng.ITimeoutService, $window: Window) {
                    var di: ng.IDirective = {
                        restrict: "A",
                        link: function(scope, element, attrs) {
                            var ratioStr : string = attrs["mdAutoHeight"];                            
                            var components = ratioStr.split(",");
                            
                            var landscapeRatio : number, portrailRatio : number
                            if (components.length > 1) {
                                portrailRatio = Number(components[0]);
                                landscapeRatio = Number(components[1]);
                            } else {
                                portrailRatio = Number(components[0]);
                                landscapeRatio = Number(components[0]);
                            }

                            if (portrailRatio === NaN || landscapeRatio === NaN) {
                                throw new Error("value is not a number");
                            }

                            function updateHeight() {
                                $timeout(function() {
                                    if ($window.innerWidth > innerHeight) {
                                        element.height(element.width() * landscapeRatio);
                                    } else {
                                        element.height(element.width() * portrailRatio);
                                    }
                                    
                                }, 500, false);
                            }

                            $window.addEventListener("resize", _.throttle(function() {
                                updateHeight();
                            }, 200));

                            updateHeight();
                        }
                    };

                    return di;
                }
            ]);
    }
}