module tranduytrung.directives {
    module mdIsCollapsed {
        import md = angular.material;
        import ng = angular;

        var module = ng.module("mdIsCollapsed", []);
        module.directive("mdIsCollapsed",
            ["$timeout",
                function ($timeout) {
                    var di: ng.IDirective = {
                        restrict: "A",
                        link: function (scope, element, attrs) {
                            var propertyName = attrs["mdIsCollapsed"];
                            if (!propertyName) {
                                throw new Error("no property is bounded");
                            }

                            var enableTransition = {
                                overflow: "hidden",
                                transitionProperty: "height",
                                transitionDuration: "0.3s",
                                transitionTimingFunction: "ease-in-out"
                            };

                            var disableTransition = {
                                transitionProperty: "",
                                transitionDuration: "",
                                transitionTimingFunction: ""
                            };

                            function getOriginalHeight() {
                                element.css(disableTransition);
                                element.css("height", "");
                                var originalHeight = element.height();
                                element.css("height", "0px");
                                element.css(enableTransition);
                                return originalHeight;
                            }

                            var isCollapsed = false;
                            function setHeight() {
                                var newHeight = isCollapsed ? 0 : getOriginalHeight();
                                $timeout(function () {
                                    element.css("height", newHeight + "px");
                                }, 0, false);
                            }

                            scope.$watch(propertyName, function (newIsCollapsed: boolean) {
                                isCollapsed = newIsCollapsed;
                                setHeight();
                            });

                            element.css(enableTransition);
                        }
                    };

                    return di;
                }
            ]);
    }
}