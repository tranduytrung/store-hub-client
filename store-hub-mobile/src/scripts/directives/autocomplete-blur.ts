/// <reference path="../index.ts" />

module StoreHub {
    module AutocompleteBlur {
        import md = angular.material;
        import ng = angular;

        storehub.directive("autocompleteBlur", ["$timeout", "$parse",
            function($timeout: ng.ITimeoutService, $parse: ng.IParseService) {
                return {
                    link(scope : ng.IScope, element : ng.IAugmentedJQuery, attrs : ng.IAttributes) {
                        var exp = $parse(attrs["autocompleteBlur"]);
                        $timeout(() => {
                            element.find("input").on("blur", () => {
                                exp(scope);
                            });
                        });
                    }
                };
            }
        ]);
    }
}