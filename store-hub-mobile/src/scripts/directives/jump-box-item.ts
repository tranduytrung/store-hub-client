/// <reference path="../index.ts" />

module StoreHub {
    module JumpBoxItem {
        import md = angular.material;
        import ng = angular;

        storehub.directive("mdJumpBoxItem",
            ["$timeout", "$parse",
                function($timeout: ng.ITimeoutService, $parse: ng.IParseService) {
                    var di: ng.IDirective = {
                        transclude: true,
                        scope: {

                        },
                        template: templates["jump-box-item"](),
                        require: "^mdJumpBox",
                        compile: function(element, attr) {
                            return postLink;
                        }
                    };

                    function postLink(scope: ng.IScope, element: ng.IAugmentedJQuery,
                        attr: ng.IAttributes,
                        ctrl: JumpBoxController) {
                        var data = ctrl.insertItem(element, scope);

                        scope["isAtLeft"] = function() {
                            return data.isAtLeft();
                        }

                        scope["isAtRight"] = function() {
                            return data.isAtRight();
                        }

                        scope["isAtActive"] = function() {
                            return data.isActive();
                        }

                        scope.$on("$destroy", function() {
                            ctrl.selectedIndex = 0;
                            ctrl.removeItem(data);
                        });
                    }

                    return di;
                }
            ]);
    }
}