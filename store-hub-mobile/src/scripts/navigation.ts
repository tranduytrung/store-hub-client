
/// <reference path="index.ts""/>

module StoreHub {
    import ui = angular.ui;

    export module Navigation {
        var _stateService: ui.IStateService;
        var _history: Array<{ name: string, params: Object }> = [];
        var _suppressHistory = false;

        storehub.run(["$rootScope", "$state",
            function($rootScope: ng.IRootScopeService, $state: ui.IStateService) {
                _stateService = $state;

                $rootScope.$on('$stateChangeSuccess',
                    function(ev, to: ui.IState, toParams: Object, from: ui.IState, fromParams: Object) {
                        if (_suppressHistory || _.isEmpty(from.name)) {
                            _suppressHistory = false;
                            return;
                        }

                        _history.unshift({
                            name: from.name,
                            params: fromParams
                        });
                    });
            }]
        );

        export function canGoBack(): boolean {
            return _history.length > 0;
        }

        export function goBack(): void {
            if (!canGoBack()) {
                throw new Error("cannot go back");
            }

            var state = _history.shift();
            _suppressHistory = true;
            _stateService.go(state.name, state.params);
        }

        export function go(name: string, params?: Object, suppressHistory: boolean = false): void {
            _suppressHistory = suppressHistory;
            _stateService.go(name, params);
        }
    }
}