
module StoreHub {
    import ui = angular.ui;
    import md = angular.material;

    export module Util {
        export function clamp(value: number, min: number, max: number) {
            return isNaN(value) || value < min ? min : value > max ? max : value;
        }

        export function wait(miliseconds: number): Promise<void> {
            return new Promise<void>(function (resolve) {
                setTimeout(resolve, miliseconds);
            });
        }

        interface ISemaphoreFunction<R> {
            (): Promise<R>
        }

        interface ISemaphoreOptions {
            sleepTime?: number
        }

        export class Semaphore {
            entries: number

            constructor(entries: number) {
                this.entries = entries;
            }

            lock<R>(func: ISemaphoreFunction<R>, options?: ISemaphoreOptions): Promise<R> {
                options = _.defaults(options, {
                    sleepTime: 100
                });

                return new Promise<R>((resolve, reject) => {
                    var tryToFire = () => {
                        if (this.entries <= 0) {
                            return setTimeout(tryToFire, options.sleepTime);
                        }

                        this.entries--;
                        func().then(result => {
                            this.entries++;
                            resolve(result);
                        }, error => {
                            this.entries++;
                            reject(error);
                        });
                    }

                    tryToFire();
                });
            }
        }

        export function getRelativeDateByDays(days : number, base? : Date) {
            var date : Date = base? new Date(base.getTime()) : new Date();
            date.setDate(date.getDate() + days);
            return date;
        }

        export function getRelativeDateByMonths(months : number, base? : Date) {
            var date : Date = base? new Date(base.getTime()) : new Date();
            date.setMonth(date.getMonth() + months);
            return date;
        }

        export function beautifyAsLowerCase(str : string) {
            if (!_.isString(str)) return "";
            str = str.trim();
            var output = str[0];
            for (var index = 1; index < str.length; index++) {
                var c = str[index];
                if (c === c.toUpperCase()) {
                    output += ` ${c.toLowerCase()}`
                } else {
                    output += c;
                }
            }

            return output;
        }
    }
}