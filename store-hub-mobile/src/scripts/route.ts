
/// <reference path="index.ts""/>

module StoreHub {
    import ui = angular.ui;
    import md = angular.material;

    storehub.config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider: ui.IStateProvider,
            $urlRouterProvider: ui.IUrlRouterProvider) {

            $urlRouterProvider.when("/lobby", "/lobby/ad");
            $stateProvider
                .state("lobby", {
                    url: "/lobby",
                    template: templates["lobby"](),
                    controller: "LobbyController",
                    controllerAs: "l"
                })
                .state("lobby.newArrivals", {
                    url: "/new-arrivals",
                    data: {
                        title: "new arrivals"
                    },
                    template: templates["product-list-simple"](),
                    controller: "NewArrivalsController",
                    controllerAs: "pl"
                })
                .state("lobby.wishlist", {
                    url: "/wishlist",
                    template: templates["product-list-simple"](),
                    controller: "WishlistController",
                    controllerAs: "pl"
                })
                .state("lobby.search", {
                    url: "/search?q",
                    data: {
                        title: "search"
                    },
                    template: templates["group-list"](),
                    controller: "SearchController",
                    controllerAs: "gl"
                })
                .state("lobby.options", {
                    url: "/product/{id:[^/]+}/options",
                    template: templates["product-options"](),
                    controller: "ProductOptionsController",
                    controllerAs: "po"
                })
                .state("lobby.price-history", {
                    url: "/product/{id:[^/]+}/price-history",
                    template: templates["price-history"](),
                    controller: "PriceHistoryController",
                    controllerAs: "ph"
                })
                .state("lobby.product", {
                    url: "/product/{groupId:[^/]+}/{productId:[^/]+}",
                    template: templates["product-detail"](),
                    controller: "ProductDetailController",
                    controllerAs: "pd"
                })
                .state("lobby.productsOfCategory", {
                    url: "/category/{category:[^/]+}",
                    template: templates["group-list-simple"](),
                    controller: "ProductsOfCategoryController",
                    controllerAs: "gl"
                })
                .state("lobby.ad", {
                    url: "/ad",
                    template: templates["ad"](),
                    controller: "AdController",
                    controllerAs: "a"
                })
                .state("lobby.user", {
                    url: "/user?redirect",
                    template: templates["user"](),
                    controller: "UserController",
                    controllerAs: "u"
                })
                .state("lobby.recent", {
                    url: "/recent",
                    template: templates["recent"](),
                    controller: "RecentController",
                    controllerAs: "r"
                })
                .state("lobby.setting", {
                    url: "/setting",
                    template: templates["setting"](),
                    controller: "SettingController",
                    controllerAs: "s"
                })

            $urlRouterProvider.otherwise("/lobby/ad");
        }]
    );
}