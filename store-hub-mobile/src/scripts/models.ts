/// <reference path="index.ts""/>

module StoreHub {
    export module Models {
        export enum ProductSortType {
            relevance = 0,
            lowPrice = 1,
            highPrice = 2,
            newest = 3,
            oldest = 4
        }

        export enum ProductCategory {
            other, phone, tablet, laptop
        }

        export enum MediaType {
            other, thumbnail, image, video
        }

        export interface IProductMedia {
            url: string,
            type: MediaType
        }

        export interface IPricePoint {
            date: Date,
            value: number
        }

        export interface IProductBrief {
            _id: string,
            source: string,
            name: string,
            url: string,
            price: number,
            priceUnit: string,
            media: IProductMedia[],
            category: ProductCategory,
            groupId: string
            updatedDate: Date
            deletedDate: Date
        }

        export interface IProduct {
            _id: string,
            groupId: string,
            source: string,
            name: string,
            url: string,
            prices: IPricePoint[],
            priceUnit: string,
            media: IProductMedia[],
            category: ProductCategory
            description: string,
            specification: Object,
            createdDate: Date,
            updatedDate: Date,
            deletedDate?: Date
        }

        export interface IProductGroup {
            _id: string,
            products: IProductBrief[]
        }

        export enum ConditionalVerbs {
            Below,
            ReducedBy
        }

        export interface ITrack {
            cycle: number
            conditionalVerb: ConditionalVerbs
            thresholdValue: number
        }

        export interface IWishItem {
            productId: string
            track?: ITrack
        }

        export interface IUser {
            _id?: string,
            firstName?: string,
            lastName?: string,
            email: string,
            password?: string,
            wishlist?: IWishItem[],
            facebookId?: string,
            googleId?: string,
            onesignalIds: string[]
        }

        export interface IAd {
            _id?: string,
            url: string,
            source: string,
            image: string
        }
    }
}

