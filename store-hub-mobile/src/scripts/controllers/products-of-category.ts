module StoreHub {
    import md = angular.material;
    import ng = angular;

    class ProductsOfCategoryController {
        isQuerying: boolean = false
        canQuery: boolean = true
        category: Models.ProductCategory
        availableSorts = Models.ProductSortType
        availableSources = ["fptshop", "lazada", "tiki", "thegioididong"]
        groups: Models.IProductGroup[] = []
        user: Models.IUser

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $stateParams: ng.ui.IStateParamsService,
            private ProductService: Services.ProductService,
            private UserService: Services.UserService,
            private AsyncService: Services.AsyncService) {

            this.category = $stateParams["category"];
            AsyncService.run(async () => {
                this.user = await UserService.getUser(false);
            });
            this.loadMore();
        }

        loadMore() {
            if (this.isQuerying || !this.canQuery) {
                return;
            }
            this.isQuerying = true;

            this.AsyncService.run(
                async () => {
                    var data = await this.ProductService.search({
                        categories: [this.category],
                        skip: this.groups.length
                    });
                    if (data.length === 0) {
                        this.canQuery = false;
                    } else {
                        Array.prototype.push.apply(this.groups, data);
                    }
                },
                {
                    errorText: "Failed to load products"
                }
            ).catch(err => {
                this.$log.error("fail load to products");
                this.$log.error(err);
            }).then(() => {
                this.isQuerying = false;
                return this.$scope.$applyAsync();
            });
        }

        get selectedSources() {
            var selected = this.ProductService.settings.search.sources;
            return selected || this.availableSources;
        }

        set selectedSources(value) {
            this.ProductService.settings.search.sources = !value || value.length === this.availableSources.length ? null : value;
        }

        get selectedSort() {
            return this.ProductService.settings.search.sort;
        }

        set selectedSort(value) {
            this.ProductService.settings.search.sort = value;
        }

        getSelectedSourcesText() {
            var selected = this.selectedSources;
            return selected.length === this.availableSources.length ? "all" : this.selectedSources.join(", ");
        }

        selectNoneSources() {
            this.selectedSources = [];
        }

        selectAllSources() {
            this.selectedSources = null;
        }

        updateSearch() {
            this.canQuery = true;
            this.groups = [];
            this.loadMore();
        }

        trackingStatus(group: Models.IProductGroup) {
            if (!this.user) return 0;
            var wishItem = _.find(this.user.wishlist, (wishItem) => {
                return _.some(group.products, ["_id", wishItem.productId]);
            });

            return wishItem ? wishItem.track ? 2 : 1 : 0;
        }
    }

    storehub.controller("ProductsOfCategoryController",
        ["$scope", "$log", "$stateParams", "ProductService", "UserService", "AsyncService",
            ProductsOfCategoryController]);
}