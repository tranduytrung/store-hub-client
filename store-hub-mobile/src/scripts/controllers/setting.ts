module StoreHub {
    import ng = angular;
    import ui = angular.ui;

    class SettingController {
        recent: IRecentSetting
        theme: IThemeSetting

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private RecentService: StoreHub.Services.RecentService,
            private ThemeService: StoreHub.Services.ThemeService,
            private AsyncService: Services.AsyncService) {

            this.loadRecentSetting();
            this.loadThemeSetting();
        }

        async loadRecentSetting() {
            this.AsyncService.run(
                async () => {
                    var clear = async () => {
                        this.recent.clear = null;
                        await this.$scope.$applyAsync();
                        await this.RecentService.clear();
                        await this.loadRecentSetting();
                    }

                    this.recent = {
                        count: await this.RecentService.count(),
                        clear
                    };

                    await this.$scope.$applyAsync();
                },
                {
                    errorText: "Failed to load settings"
                }
            ).catch(err => {
                this.$log.error("Failed to load settings");
            });
        }

        loadThemeSetting() {
            var ThemeService = this.ThemeService;
            this.theme = {
                currentTheme: ThemeService.current,
                themes: ThemeService.getThemes(),
                set currentThemeId(id: string) {
                    ThemeService.setTheme(id);
                },
                get currentThemeId() {
                    return ThemeService.current.id;
                }
            };
        }
    }

    interface IRecentSetting {
        count: number
        clear(): Promise<void>
    }

    interface IThemeSetting {
        themes: Services.IThemeObject[]
        currentTheme: Services.IThemeObject
        currentThemeId: string
    }

    storehub.controller("SettingController",
        ["$scope", "$log", "RecentService", "ThemeService", "AsyncService",
            SettingController]);
}