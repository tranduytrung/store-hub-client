/// <reference path="../index.ts" />

module StoreHub {
    import md = angular.material;
    import ng = angular;

    class SearchController {
        q: string
        availableSources = ["fptshop", "lazada", "tiki", "thegioididong"]
        availableSorts = Models.ProductSortType

        isQuerying: boolean = false;
        canQuery: boolean = true;
        groups: Models.IProductGroup[] = []
        user: Models.IUser

        constructor(private $scope: ng.IScope,
            private $stateParams: ng.ui.IStateParamsService,
            private $log: ng.ILogService,
            private ProductService: Services.ProductService,
            private UserService: Services.UserService,
            private AsyncService: Services.AsyncService) {
            this.q = decodeURIComponent($stateParams["q"]);

            AsyncService.run(async () => {
                this.user = await UserService.getUser(false);
            });

            this.loadMore();
        }

        get selectedSources() {
            var selected = this.ProductService.settings.search.sources;
            return selected || this.availableSources;
        }

        set selectedSources(value) { 
            this.ProductService.settings.search.sources = !value || value.length === this.availableSources.length? null : value;
        }

        get selectedSort() {
            return this.ProductService.settings.search.sort;
        }

        set selectedSort(value) {
            this.ProductService.settings.search.sort = value;
        }

        getSelectedSourcesText() {
            var selected = this.selectedSources;
            return selected.length === this.availableSources.length ? "all" : this.selectedSources.join(", ");
        }

        selectNoneSources() {
            this.selectedSources = [];
        }

        selectAllSources() {
            this.selectedSources = null;
        }

        updateSearch() {
            this.canQuery = true;
            this.groups = [];
            this.loadMore();
        }

        loadMore() {
            if (this.isQuerying || !this.canQuery) {
                return;
            }
            this.isQuerying = true;
            return this.AsyncService.run(
                async () => {
                    if (!_.isEmpty(URI(this.q).hostname())) {
                        var product = await this.ProductService.getProductByUrl(this.q);
                        this.groups.push({
                            _id: product.groupId,
                            products: [product]
                        })
                        this.canQuery = false;
                    } else {
                        var data = await this.ProductService.search({
                            q: this.q,
                            skip: this.groups.length
                        });
                        if (data.length === 0) {
                            this.canQuery = false;
                        } else {
                            Array.prototype.push.apply(this.groups, data);
                        }
                    }
                },
                {
                    runningText: "searching...",
                    errorText: "Failed to load search results"
                }
            ).then(_.noop).catch(err => {
                this.$log.error("Failed to load search result");
                this.$log.error(err);
            }).then(() => {
                this.isQuerying = false;
                return this.$scope.$applyAsync();
            });
        }

        trackingStatus(group: Models.IProductGroup) {
            if (!this.user) return 0;
            var wishItem = _.find(this.user.wishlist, (wishItem) => {
                return _.some(group.products, ["_id", wishItem.productId]);
            });

            return wishItem ? wishItem.track ? 2 : 1 : 0;
        }
    }

    storehub.controller("SearchController",
        ["$scope", "$stateParams", "$log", "ProductService", "UserService", "AsyncService",
            SearchController]);
}