/// <reference path="../index.ts" />

module StoreHub {
    import md = angular.material;
    import ng = angular;
    import ui = angular.ui;

    /**
     * LobbyController
     */
    class LobbyController {
        previousState: { name: string, params: any };
        searchText: string

        constructor(private $mdSidenav: md.ISidenavService,
            private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $state: ng.ui.IStateService) {
            $scope.$on("$stateChangeSuccess",
                (ev, to: ui.IState, params) => {
                    if (to.name !== "lobby.search") {
                        this.searchText = "";
                    } else {
                        this.searchText = decodeURIComponent(params["q"]);
                    }
                });

            if ($state.current.name === "lobby.search") {
                this.searchText = decodeURIComponent($state.params["q"]);
            }
        }

        openLeftNavbar() {
            this.$mdSidenav("left").open();
        }
        
        closeLeftNavbar() {
            this.$mdSidenav("left").close();
        }
        
        canBack() {
            return Navigation.canGoBack();
        }
        
        back() {
            Navigation.goBack();
        }

        search = _.debounce(() => {
            var isSearchState = this.$state.is("lobby.search");
            if (_.isEmpty(this.searchText)) {
                if (isSearchState && this.previousState) {
                    Navigation.go(this.previousState.name, this.previousState.params, true);
                }

                return;
            }

            if (!isSearchState) {
                this.previousState = {
                    name: this.$state.current.name,
                    params: this.$state.params
                }
            }

            Navigation.go("lobby.search", {
                q: encodeURIComponent(this.searchText)
            }, true);
        }, 500);
    }

    storehub.controller("LobbyController",
        ["$mdSidenav", "$scope", "$log", "$state",
            LobbyController
        ]);
}