module StoreHub {
    import ng = angular;
    import ui = angular.ui;

    class RecentController {
        products: IProductWithAge[]

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private RecentService: StoreHub.Services.RecentService,
            private ProductService: StoreHub.Services.ProductService,
            private AsyncService: Services.AsyncService) {

            this.loadRecentProduct();
        }

        loadRecentProduct() {
            return this.AsyncService.run(
                async () => {
                    var items = await this.RecentService.getRecentItems();
                    if (items.length === 0) return;
                    var ids = _.map<any, string>(items, "id");
                    var products = await this.ProductService.getProducts(ids);
                    var productWithAges = _.map<any, IProductWithAge>(items, (item) => {
                        var product : any = _.find(products, ["_id", item.id]);
                        if (!product) return null;
                        product["age"] = item["date"];
                        return product;
                    });

                    this.products = _.reject(productWithAges, _.isNil);
                    await this.$scope.$applyAsync();
                },
                {
                    errorText: "Failed to load recent products"
                }
            ).catch(err => {
                this.$log.error("Failed to load recent products");
            });
        }
    }

    storehub.controller("RecentController",
        ["$scope", "$log", "RecentService", "ProductService", "AsyncService",
            RecentController]);

    interface IProductWithAge extends Models.IProductBrief {
        age: Date
    }
}