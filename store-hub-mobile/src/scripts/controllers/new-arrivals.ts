module StoreHub {
    import md = angular.material;
    import ng = angular;

    class NewArrivalsController {
        products: Models.IProduct[]
        isQuerying: boolean = false;
        canQuery: boolean = true;

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private ProductService: Services.ProductService,
            private AsyncService: Services.AsyncService) {

            AsyncService.run(
                async () => {
                    this.products = await ProductService.getNewArrivals();
                    await $scope.$applyAsync();
                },
                {
                    errorText: "Failed to load new arrivals"
                }
            ).catch(err => {
                $log.error("Failed to load new arrivals");
            });
        }

        loadMore() {
            if (this.isQuerying || !this.canQuery) {
                return;
            }
            this.isQuerying = true;

            this.AsyncService.run(
                async () => {
                    var data = await this.ProductService.getNewArrivals(_.last(this.products)._id);
                    if (data.length === 0) {
                        this.canQuery = false;
                    } else {
                        Array.prototype.push.apply(this.products, data);
                    }

                    this.$scope.$applyAsync();
                },
                {
                    errorText: "Failed to load more products"
                }
            ).catch(err => {
                this.$log.error("fail load to more products");
                this.$log.error(err);
            }).then(() => {
                this.isQuerying = false;
            });
        }
    }

    storehub.controller("NewArrivalsController",
        ["$scope", "$log", "ProductService", "AsyncService",
            NewArrivalsController]);
}