/// <reference path="../index.ts" />

module StoreHub {
    import md = angular.material;
    import ng = angular;

    export class JumpBoxItem {
        constructor(public $scope : ng.IScope, 
        public $element: ng.IAugmentedJQuery, 
        public controller: JumpBoxController) {
            
        }
        
        get index() : number {
            return this.controller.getElementIndex(this.$element);
        }
        
        isAtLeft(): boolean {
            return this.index < this.controller.selectedIndex;
        }
        
        isAtRight(): boolean {
            return this.controller.selectedIndex < this.index;
        }
        
        isActive(): boolean {
            return this.controller.selectedIndex === this.index;
        }
    }

    export class JumpBoxController {
        selectedIndex: number = 0;
        items: JumpBoxItem[] = [];
        showIndicator: boolean;

        constructor(private $scope: ng.IScope,
            private $element: ng.IAugmentedJQuery, private $attrs: ng.IAttributes) {
                this.showIndicator = !_.isEmpty(this.$attrs["mdJumpBoxIndicator"]);
        }

        insertItem(element: ng.IAugmentedJQuery, scope: ng.IScope): JumpBoxItem {
            var item = new JumpBoxItem(scope, element, this);
            this.items.push(item);             
            return item;
        }
        
        removeItem(item : JumpBoxItem) {
            var index = this.items.indexOf(item);
            
            // Do something if deleting index is selectedIndex
            
            this.items.splice(index, 1);
        }

        getElementIndex($child: ng.IAugmentedJQuery): number {
            var children = this.$element[0].getElementsByTagName("md-jump-box-item");
            return Array.prototype.indexOf.call(children, $child[0]);
        }
        
        goNext() {
            if (this.selectedIndex === this.items.length - 1) return;
            this.selectedIndex++;
        }
        
        goPrevious() {
            if (this.selectedIndex === 0) return;
            this.selectedIndex--;
        }
    }

    storehub.controller("JumpBoxController",
        ["$scope", "$element", "$attrs", JumpBoxController]);
}