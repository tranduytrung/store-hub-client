module StoreHub {
    import md = angular.material;
    import ng = angular;

    class PriceHistoryController {
        product: Models.IProduct
        periods: { [name: string]: number }
        selectedPeriod: number

        constructor(private $scope: ng.IScope,
            private $element: ng.IAugmentedJQuery,
            private $log: ng.ILogService,
            private $stateParams: ng.ui.IStateParamsService,
            private ProductService: StoreHub.Services.ProductService,
            private AsyncService: Services.AsyncService) {

            var id = $stateParams["id"];
            this.updatePeriods(new Date());

            AsyncService.run(
                async () => {
                    this.product = await ProductService.getProduct(id);
                    this.nvd3Options.title.text = this.product.name;

                    var lastDate = this.product.prices[0].date;
                    this.updatePeriods(lastDate);
                    this.selectedPeriod = this.periods["1 month"];

                    this.nvd3data = [{
                        values: this.product.prices,
                        key: this.product.name
                    }];

                    await $scope.$applyAsync();
                    setTimeout(() => {
                        var nvd3 = this.$element.find("nvd3");
                        this.nvd3Options.chart.height = nvd3.width() * 0.5625;
                        this.updateDomain();
                        this.nvd3Api.update();
                    });
                },
                {
                    errorText: "Unable to load price history"
                }
            ).catch(err => {
                this.$log.error("Fail to load price history");
            });
        }

        updateDomain() {
            var lastDate = this.product.prices[0].date;
            this.nvd3Options.chart.xDomain[0] = this.selectedPeriod;
            this.nvd3Options.chart.xDomain[1] = lastDate.getTime();

            var index = _.findIndex(this.product.prices, (p) => {
                return p.date.getTime() < this.selectedPeriod;
            });

            var items = index < 0 ? this.product.prices : _.slice(this.product.prices, 0, index);
            var min: number = _.minBy(items, "value").value;
            var max: number = _.maxBy(items, "value").value;
            var margin = (max - min) * 0.2;
            this.nvd3Options.chart.yDomain[0] = min - margin;
            this.nvd3Options.chart.yDomain[1] = max + margin;
        }

        updatePeriods(base: Date) {
            this.periods = {
                "7 days": Util.getRelativeDateByDays(-7, base).getTime(),
                "14 days": Util.getRelativeDateByDays(-14, base).getTime(),
                "1 month": Util.getRelativeDateByMonths(-1, base).getTime(),
                "3 months": Util.getRelativeDateByMonths(-3, base).getTime(),
                "6 months": Util.getRelativeDateByMonths(-6, base).getTime()
            }
        }

        nvd3Api
        nvd3data = []
        nvd3Options = {
            title: {
                enable: true,
                text: "",
                className: "md-headline"
            },
            chart: {
                type: "lineChart",
                height: 400,
                //useInteractiveGuideline: true,
                margin: {
                    right: 38,
                    left: 80
                },
                showLegend: false,
                xDomain: [0, 0],
                yDomain: [0, 0],
                x: function (d) {
                    if (!d || !d.date) return NaN;
                    return d.date.getTime();
                },
                y: function (d) {
                    if (!d || !d.value) return NaN;
                    return d.value;
                },
                xAxis: {
                    tickFormat: function (d) {
                        return d3.time.format("%x")(new Date(d));
                    },
                    tickPadding: 20
                },
                yAxis: {
                    tickFormat: function (d) {
                        return d3.format(",")(d);
                    }
                }
            }
        }
    }

    storehub.controller("PriceHistoryController",
        ["$scope", "$element", "$log", "$stateParams", "ProductService", "AsyncService",
            PriceHistoryController]);
}