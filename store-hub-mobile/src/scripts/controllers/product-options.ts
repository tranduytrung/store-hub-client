module StoreHub {
    import md = angular.material;
    import ng = angular;

    class ProductOptionsController {
        loaded: boolean = false

        cycleOptions = {
            0: "auto",
            12: "12 hours",
            24: "24 hours",
            36: "36 hours",
            48: "24 hours",
            60: "60 hours",
            72: "72 hours"
        }

        conditionalVerbOptions = {
            [Models.ConditionalVerbs.Below]: "below",
            [Models.ConditionalVerbs.ReducedBy]: "reduced by"
        }

        conditionalValueInput: string = "1"

        product: Models.IProduct
        wishItem: Models.IWishItem

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $stateParams: ng.ui.IStateParamsService,
            private ProductService: StoreHub.Services.ProductService,
            private UserService: StoreHub.Services.UserService,
            private AsyncService: Services.AsyncService) {
            var id = $stateParams["id"];

            AsyncService.run(
                async () => {
                    this.product = await ProductService.getProduct(id);
                    var user = await UserService.getUser();
                    var wishItem = _.find(user.wishlist, {
                        productId: id
                    });

                    if (wishItem && wishItem.track) {
                        var price = _.first(this.product.prices).value;

                        this.wishItem = wishItem;
                        this.conditionalValueInput = (wishItem.track.conditionalVerb === Models.ConditionalVerbs.ReducedBy ?
                            price - wishItem.track.thresholdValue : wishItem.track.thresholdValue).toString();
                    } else {
                        this.wishItem = {
                            track: {
                                cycle: 0,
                                thresholdValue: 1,
                                conditionalVerb: Models.ConditionalVerbs.Below,
                            },
                            productId: id
                        }

                        this.conditionalValueInput = "90%"
                    }

                    this.loaded = true;
                    await $scope.$applyAsync();
                },
                {
                    errorText: "Unable to load tracking status"
                }
            ).catch(err => {
                this.$log.error("Fail load tracking status");
            });
        }

        validateConditionalValue() {
            var price = _.first(this.product.prices).value;

            // if is percentage
            var conditionalValue;
            if (_.last(this.conditionalValueInput) === "%") {
                conditionalValue = Number(this.conditionalValueInput.substr(0, this.conditionalValueInput.length - 1)) * price / 100;
            } else {
                conditionalValue = Number(this.conditionalValueInput);
            }

            this.wishItem.track.thresholdValue = this.wishItem.track.conditionalVerb === Models.ConditionalVerbs.ReducedBy ?
                price - conditionalValue : conditionalValue;
            this.wishItem.track.cycle = Number(this.wishItem.track.cycle);
            this.wishItem.track.conditionalVerb = Number(this.wishItem.track.conditionalVerb);
        }

        setTrack() {
            return this.AsyncService.run(
                async () => {
                    this.validateConditionalValue();
                    await this.UserService.track(this.wishItem);
                    if (Navigation.canGoBack()) {
                        Navigation.goBack();
                    } else {
                        Navigation.go("lobby.product", {
                            groupId: this.product.groupId,
                            productId: this.product._id
                        }, true);
                    }
                },
                {
                    runningText: "Updating tracking status",
                    errorText: "Unable to update tracking status"
                }
            ).catch(err => {
                this.$log.error("Fail update tracking status");
            });
        }
    }

    storehub.controller("ProductOptionsController",
        ["$scope", "$log", "$stateParams", "ProductService", "UserService", "AsyncService",
            ProductOptionsController]);
}