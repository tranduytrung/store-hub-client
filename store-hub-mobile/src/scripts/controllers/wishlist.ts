module StoreHub {
    import md = angular.material;
    import ng = angular;

    class WishlistController {
        user: Models.IUser
        products: Models.IProductBrief[]

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private ProductService: Services.ProductService,
            private UserService: Services.UserService,
            private AsyncService: Services.AsyncService) {

            AsyncService.run(async () => {
                this.user = await UserService.getUser();
                var ids = _.map(this.user.wishlist, function (elem) {
                    return elem.productId;
                });

                this.products = await ProductService.getProducts(ids);
                await $scope.$applyAsync();
            }, {
                errorText: "Unable to load wishlist"
            }).catch(err => {
                $log.error("Fail to get wishlist");
            });
        }
    }

    storehub.controller("WishlistController",
        ["$scope", "$log", "ProductService", "UserService", "AsyncService",
            WishlistController]);
}