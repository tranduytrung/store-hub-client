module StoreHub {
    import md = angular.material;
    import ng = angular;

    class ProductDetailController {
        products: Models.IProduct[]
        product: Models.IProduct
        user: Models.IUser
        isWished: boolean
        showPriceHistory: boolean = false

        isQuerying: boolean = false
        canQuery: boolean = true
        pageIndex: number = -1

        constructor(private $scope: ng.IScope,
            private $window: ng.IWindowService,
            private $state: ng.ui.IStateService,
            private $stateParams: ng.ui.IStateParamsService,
            private $log: ng.ILogService,
            private ProductService: Services.ProductService,
            private UserService: Services.UserService,
            private RecentService: Services.RecentService,
            private AsyncService: Services.AsyncService) {

            var groupId = $stateParams["groupId"];
            var productId = $stateParams["productId"];

            AsyncService.run(
                async () => {
                    var product = await ProductService.getProduct(productId);
                    this.products = [product];
                    await this.RecentService.set(productId);

                    this.selectProduct(productId);

                    this.user = await UserService.getUser(false);
                    this.updateIsWished();
                    await $scope.$applyAsync();
                },
                {
                    errorText: "Failed to load product detail"
                }
            ).catch(err => {
                $log.error("Failed to load product detail");
            });

            this.loadMoreSimilarProducts();
        }

        loadMoreSimilarProducts() {
            if (this.isQuerying || !this.canQuery) {
                return;
            }
            this.isQuerying = true;

            return this.AsyncService.run(
                async () => {
                    var data = await this.ProductService.getProductsByGroupId(this.product.groupId, ++this.pageIndex, 6);
                    if (data.length === 0) {
                        this.canQuery = false;
                    } else {
                        if (data.length < 6) {
                            this.canQuery = false;
                        }
                        Array.prototype.push.apply(this.products, _.reject(data, ["_id", this.products[0]._id]));
                    }
                },
                {
                    errorText: "Fail load to more similar products"
                }
            ).catch(err => {
                --this.pageIndex;
                this.$log.error("Fail load to more similar products");
                this.$log.error(err);
            }).then(() => {
                this.isQuerying = false;
                this.$scope.$applyAsync();
            });
        }

        selectProduct(productId: string) {
            this.product = _.find(this.products, ["_id", productId]);
            this.$state.go(".",
                {
                    productId: productId
                },
                {
                    notify: false
                }
            );

            this.updateIsWished();
            this.nvd3Api.updateWithData(this.product.prices);
        }

        isTracking(productId?: string) {
            if (!this.user) return false;
            var wishItem = _.find(this.user.wishlist, ["productId", productId || this.product._id]);
            if ((wishItem && wishItem.track)) return true;

            return false;
        }

        trackingStatus(productId?: string) {
            if (!this.user) return 0;
            var wishItem =  _.find(this.user.wishlist, ["productId", productId || this.product._id]);

            return wishItem? wishItem.track? 2 : 1 : 0;
        }

        private updateIsWished() {
            if (!this.user) {
                this.isWished = false;
                return;
            }

            var wishItem = _.find(this.user.wishlist, ["productId", this.product._id]);
            this.isWished = !!wishItem;
        }

        async toggleWish() {
            this.AsyncService.run(
                async () => {
                    if (!this.user) {
                        this.user = await this.UserService.getUser();
                        return;
                    }

                    if (this.isWished) {
                        await this.UserService.unwish(this.product._id);
                    } else {
                        await this.UserService.wish(this.product._id);
                    }
                    this.user = await this.UserService.getUser();
                    this.updateIsWished();

                    await this.$scope.$applyAsync();
                },
                {
                    runningText: "Updating wish status",
                    errorText: "Unable to update wish status"
                }).catch(err => {
                    this.$log.error("Fail update wish status");
                });
        }

        navigateToOptions() {
            StoreHub.Navigation.go("lobby.options", {
                id: this.product._id
            });
        }

        async untrack() {
            this.AsyncService.run(
                async () => {
                    await this.UserService.untrack(this.product._id);
                    this.user = await this.UserService.getUser();
                    await this.$scope.$applyAsync();
                },
                {
                    runningText: "Updating tracking status",
                    errorText: "Unable to update tracking status"
                }).catch(err => {
                    this.$log.error("Fail update tracking status");
                });
        }

        goToOrigin() {
            if (!this.$window["cordova"]) {
                this.$window.open(this.product.url, "_blank");
                return;
            }

            let InAppBrowser: InAppBrowser = window.cordova["InAppBrowser"];
            InAppBrowser.open(this.product.url, '_blank', 'location=no');
        }

        togglePriceHistory() {
            this.showPriceHistory = !this.showPriceHistory;
        }

        nvd3Api
        nvd3Options = {
            "chart": {
                type: "sparklinePlus",
                height: 168,
                color: ["#C6FF00"],
                x: function (d) {
                    return d.date;
                },
                y: function (d) {
                    return d.value;
                },
                xTickFormat: function (d) {
                    return d3.time.format('%d/%m/%Y')(d);
                },
                yTickFormat: function (d) {
                    return d3.format(",")(d);
                }
            }
        }
    }

    storehub.controller("ProductDetailController",
        ["$scope", "$window", "$state", "$stateParams", "$log", "ProductService", "UserService", "RecentService", "AsyncService",
            ProductDetailController
        ]);
}