module StoreHub {
    import md = angular.material;
    import ng = angular;

    class AdController {
        selectedSource: string
        sources: string[] = ["all"]
        ads: IVisualAd[]

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $window: ng.IWindowService,
            private AdService: Services.AdService,
            private AsyncService: Services.AsyncService) {

            AsyncService.run<void>(async () => {
                var sources = await this.AdService.getSourceEnumeration();
                Array.prototype.push.apply(this.sources, sources);
                await this.selectSource();
            }, {
                errorText: "Failed to load ads!"
            }).catch(err => {
                $log.error("Fail to get ads");
            });
        }

        openAd(url: string) {
            if (!this.$window["cordova"]) {
                this.$window.open(url, "_blank");
                return;
            }

            let InAppBrowser: InAppBrowser = window.cordova["InAppBrowser"];
            InAppBrowser.open(url, '_blank', 'location=no');
        }

        async selectSource(source?: string) {
            this.selectedSource = source || this.selectedSource || "all";
            var ads = await this.AdService.getAds(this.selectedSource === "all" ? null : this.selectedSource);
            this.ads = await this.convertToVisualAds(ads);
            await this.$scope.$applyAsync();
        }

        private async convertToVisualAds(ads: Models.IAd[]) {
            var vAds: IVisualAd[] = [];
            for (var index = 0; index < ads.length; index++) {
                var ad = ads[index];
                var size = await this.getImageSize(ad.image);
                var width, height;
                if (size.height > size.width) {
                    width = 1;
                    height = Util.clamp(Math.round(size.height / size.width), 1, 2);
                } else {
                    height = 1;
                    width = Util.clamp(Math.round(size.width / size.height), 1, 2);
                }
                vAds.push({
                    _id: ad._id,
                    url: ad.url,
                    image: ad.image,
                    source: ad.source,
                    width: width,
                    height: height
                });
            }

            return vAds;
        }

        private getImageSize(url: string) {
            var log = this.$log;
            return new Promise<{ width: number, height: number }>(function (resolve) {
                var img = new Image();
                img.onload = function () {
                    var size = {
                        width: img.width,
                        height: img.height
                    };
                    log.info(url, size);
                    resolve(size);
                }

                img.onerror = function () {
                    log.info(url);
                    resolve({
                        width: 0,
                        height: 0
                    });
                }

                img.src = url;
            });
        }
    }

    storehub.controller("AdController",
        ["$scope", "$log", "$window", "AdService", "AsyncService",
            AdController]);

    interface IVisualAd extends Models.IAd {
        width: number
        height: number
    }
}