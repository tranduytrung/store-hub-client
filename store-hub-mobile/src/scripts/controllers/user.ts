module StoreHub {
    import ng = angular;
    import ui = angular.ui;

    class UserController {
        user: Models.IUser

        constructor(private $scope: ng.IScope,
            private $log: ng.ILogService,
            private $location: ng.ILocationService,
            private $stateParams: ui.IStateParamsService,
            private UserService: StoreHub.Services.UserService,
            private AsyncService: Services.AsyncService) {

            this.AsyncService.run(
                async () => {
                    this.user = await UserService.getUser(false);
                    await $scope.$applyAsync();
                },
                {
                    errorText: "Failed to fletch user information"
                }
            ).catch(err => {
                $log.error("Failed to fletch user information", err);
            });
        }

        login() {
            return this.AsyncService.run(
                async () => {
                    this.user = await this.UserService.getUser();
                    await this.$scope.$applyAsync();

                    var redirect = this.$stateParams["redirect"];
                    if (redirect) {
                        this.$location.url(redirect);
                    }
                },
                {
                    runningText: "Logging in",
                    errorText: "Failed to login"
                }
            ).catch(err => {
                this.$log.error("Failed to login", err);
            });
        }

        logout() {
            return this.AsyncService.run(
                async () => {
                    this.user = null;
                    await this.UserService.logout();
                    await this.$scope.$applyAsync;
                },
                {
                    runningText: "Logging out",
                    errorText: "Failed to logout"
                }
            ).catch(err => {
                this.$log.error("Failed to logout", err);
            });
        }
    }

    storehub.controller("UserController",
        ["$scope", "$log", "$location", "$stateParams", "UserService", "AsyncService",
            UserController]);
}