﻿
// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397705
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

/// <reference path="onesignal.ts" />

module StoreHub {
    export var isTest = false;
    export let host = isTest ? "http://tranduytrung.ddns.net" : "http://storehub.mybluemix.net";

    export var storehub = angular.module("store-hub", [
        "ngMessages",
        "ui.router",
        "ngAnimate",
        "ngAria",
        "ngSanitize",
        "ngMaterial",
        "nvd3",
        "mdThemed",
        "mdIsCollapsed",
        "mdAutoHeight",
        "angularMoment"
    ]);

    export module Application {
        interface IInitializer {
            (): Promise<void>
        }

        var initializers: Promise<void>[] = [];

        export function init(func: IInitializer) {
            initializers.push(func());
        }

        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener("pause", onPause, false);
            document.addEventListener("resume", onResume, false);

            // Cordova has been loaded. Perform any initialization that requires Cordova here.
            if (window["cordova"]) {
                OneSignal.initialize();
            }
        }

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        }

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        }

        window.onload = function () {
            Promise.all(initializers).then(function () {
                document.addEventListener("deviceready", onDeviceReady, false);
                angular.bootstrap(document, ["store-hub"]);
            });
        }
    }
}