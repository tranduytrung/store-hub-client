module StoreHub {
    export module OneSignal {
        export function initialize() {
            var onesignal = window.plugins["OneSignal"];
            
            onesignal.setLogLevel({ logLevel: 6, visualLevel: 0 });
            
            // this function will be called at the time a messages is received
            var notificationOpenedCallback = function (jsonData) {
                var product = jsonData.additionalData.product;
                if (_.isEmpty(product)) {
                    return;
                }

                Navigation.go("lobby.product", {
                    groupId: product.groupId,
                    productId: product._id
                });
            };
            
            // go to product detail page
            onesignal.init(
                "3ff09ec7-cf28-487c-9141-edd25759e85f",
                {
                    googleProjectNumber: "154418566265"
                },
                notificationOpenedCallback);

            onesignal.enableNotificationsWhenActive(true);
        }

        var onesignalId = null;
        export function getOnesignalId() {
            return new Promise<string>(function (resolve, reject) {
                if (!_.isEmpty(onesignalId)) {
                    return resolve(onesignalId);
                }

                var onesignal = window.plugins["OneSignal"];
                if (!onesignal) {
                    return reject(new Error("no One Signal plugin"));
                }

                onesignal.getIds(function (ids) {
                    var id = _.get<string>(ids, "userId");
                    if (_.isEmpty(id)) {
                        return reject(new Error("no connection or user is not accept notification"));
                    }

                    onesignalId = id;
                    resolve(id);
                });
            });
        }
    }
}