module StoreHub {
    import ng = angular;

    storehub.filter("beatify", function() {
        return function(str : string) {
            return Util.beautifyAsLowerCase(str);
        };
    });
}