var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var merge2 = require('merge2');

var jade = require('gulp-jade');
var jadeConcat = require('gulp-jade-template-concat');

var ts = require('gulp-typescript');
var uglify = require('gulp-uglify');
const babel = require('gulp-babel');

var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var bourbon = require('node-bourbon');

gulp.task('jade', function () {
    var index = gulp.src('./src/index.jade')
        .pipe(jade({
            pretty: true
        }));

    var template = gulp.src('./src/templates/*.jade')
        .pipe(jade({
            client: true,
            pretty: true
        }))
        .pipe(jadeConcat("scripts/templates.js", {
            templateVariable:"templates"
        }));

    return merge2(index, template)
        .pipe(gulp.dest('./www'));
});

gulp.task('vendorcss', function () {
    var cssFiles = [
        'node_modules/angular-material/angular-material.css',
        'node_modules/nvd3/build/nv.d3.css'
    ];

    return gulp.src(cssFiles)
        .pipe(sourcemaps.init())
        .pipe(cssnano())
        .pipe(concat('www/styles/vendor.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('sass', function () {
    return gulp.src('./src/styles/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: bourbon.includePaths,
            outputStyle: 'compressed'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('www/styles'));
});

gulp.task('vendorjs', function () {
    var jsFiles = [
        'node_modules/lodash/lodash.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/angular/angular.js',
        'node_modules/angular-animate/angular-animate.js',
        'node_modules/angular-aria/angular-aria.js',
        'node_modules/angular-messages/angular-messages.js',
        'node_modules/angular-sanitize/angular-sanitize.js',
        'node_modules/angular-ui-router/build/angular-ui-router.js',
        'node_modules/angular-material/angular-material.js',
        'node_modules/d3/d3.js',
        'node_modules/nvd3/build/nv.d3.js',
        'node_modules/angular-nvd3/dist/angular-nvd3.js',
        'node_modules/urijs/src/URI.js',
        'node_modules/localforage/dist/localforage.js',
        'node_modules/moment/moment.js',
        'node_modules/angular-moment/angular-moment.js',
        "src/scripts/runtime.js"
    ];

    return gulp.src(jsFiles)
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        //.pipe(uglify()) 
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/scripts'));
});

var tsProject = ts.createProject('tsconfig.json');
gulp.task('typescript', function () {
    var tsResult = tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(ts(tsProject));

    return tsResult.js
        .pipe(concat('appBundle.js')) // You can use other plugins that also support gulp-sourcemaps
        .pipe(babel({
			presets: ['es2015']
		}))
        // .pipe(uglify()) //only for production
        .pipe(sourcemaps.write('.')) // Now the sourcemaps are added to the .js file. This should be disable for production 
        .pipe(gulp.dest('www/scripts'));
});

gulp.task('font', function () {
    return gulp.src("src/fonts/*")
        .pipe(gulp.dest("www/fonts"));
});

gulp.task('static', ["jade", "sass"], function () {
});

gulp.task('build', ["typescript", "jade", "vendorjs", "sass", "vendorcss", "font"], function () {
});

gulp.task('default', ["build"], function () {
    // place code for your default task here
});