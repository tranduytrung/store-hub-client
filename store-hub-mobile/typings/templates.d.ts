
declare module Jade {
    interface ITemplateFunction {
        (locals?: Object) : string
    }

    interface ITemplates {
        [key: string] : ITemplateFunction;
    }
}

declare var templates : Jade.ITemplates;

